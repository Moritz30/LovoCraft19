package com.allesminecraft.interfaces;

public interface IProcess {

	public void updateProcess();

	public boolean isDead();
}
