package com.allesminecraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLovoPortalFrame extends Block{

	public BlockLovoPortalFrame() {
		super(Material.rock);
		this.setHardness(25.0F);
		this.setResistance(1800.0F);
		this.setStepSound(SoundType.STONE);
	}
	public boolean isFireSource(World world, BlockPos pos, EnumFacing side) {
		if (side == EnumFacing.UP) {
			return true;
		} else {
			return false;
		}
	}
}
