package com.allesminecraft.block;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftBlocks;
import com.allesminecraft.LovoCraftItems;

public class BlockLovoCobblestoneFired extends Block{

	public BlockLovoCobblestoneFired() {
		super(Material.rock);
	}
	public boolean isFireSource(World world, BlockPos pos, EnumFacing side) {
		if (side == EnumFacing.UP) {
			return true;
		} else {
			return false;
		}
	}
	@Override
	public ArrayList<ItemStack> getDrops(IBlockAccess world, BlockPos pos, IBlockState state, int fortune) {
		ArrayList<ItemStack> drops = new ArrayList<ItemStack>();
		drops.add(new ItemStack(LovoCraftBlocks.lovocobblestone));
		drops.add(new ItemStack(LovoCraftItems.lovocrystalcrumbled));
		return drops;
	}
	@Override
	public void onLanded(World worldIn, Entity entityIn) {

		if (entityIn instanceof EntityPlayer && !worldIn.isRemote) {
			EntityPlayer player = (EntityPlayer) entityIn;
			player.setFire(5);
		}
	}
}
