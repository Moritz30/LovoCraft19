package com.allesminecraft.block;

import java.util.Random;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftBlocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLovoCobblestone extends Block {
	public BlockLovoCobblestone() {
		super(Material.rock);
		this.setHardness(1.5F);
		this.setResistance(10.0F);
		this.setStepSound(SoundType.STONE);
	}
	public boolean isFireSource(World world, BlockPos pos, EnumFacing side) {
		if (side == EnumFacing.UP) {
			return true;
		} else {
			return false;
		}
	}
}
