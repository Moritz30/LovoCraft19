package com.allesminecraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.tileentity.TileEntityLovoFurnace;


public class BlockLovoFurnace extends Block implements ITileEntityProvider{

	public static PropertyEnum bool = PropertyEnum.create("facing", EnumFacing.class, EnumFacing.Plane.HORIZONTAL);
	
	public BlockLovoFurnace() {
		super(Material.rock);
		this.setHardness(1.5F);
		this.setResistance(10.0F);
		this.setStepSound(SoundType.STONE);
        this.setDefaultState(this.blockState.getBaseState().withProperty(bool, EnumFacing.NORTH));
	}
	

	public IBlockState onBlockPlaced(World worldIn, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ, int meta, EntityLivingBase placer)
	{
	        return this.getDefaultState().withProperty(bool, placer.getHorizontalFacing().getOpposite());
	}
	
	@SideOnly(Side.CLIENT)
    public IBlockState getStateForEntityRender(IBlockState state)
    {
	        return this.getDefaultState().withProperty(bool, EnumFacing.SOUTH);
    }

    public IBlockState getStateFromMeta(int meta)
    {
	        EnumFacing enumfacing = EnumFacing.getFront(meta);

	        if (enumfacing.getAxis() == EnumFacing.Axis.Y)
	        {
	            enumfacing = EnumFacing.NORTH;
	        }

	        return this.getDefaultState().withProperty(bool, enumfacing);
    }

	   
	public int getMetaFromState(IBlockState state)
    {
	        return ((EnumFacing)state.getValue(bool)).getIndex();
	}
	
	@Override
	protected BlockStateContainer createBlockState() {
		return new BlockStateContainer(this, bool);
	}
	
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
    		EnumFacing side, float hitX, float hitY, float hitZ) {
        if(!worldIn.isRemote){
    	playerIn.openGui(LovoCraft.MODID, LovoCraft.GUI_LOVOFURNACE, worldIn, pos.getX(), pos.getY(), pos.getZ());
        }
    	return true;
    }
    

    
	@Override
	public TileEntity createNewTileEntity(World worldIn, int meta) {
		return new TileEntityLovoFurnace();
	}
	
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state)
	{
	            TileEntity tileentity = worldIn.getTileEntity(pos);

	            if (tileentity instanceof TileEntityLovoFurnace)
	            {
	                InventoryHelper.dropInventoryItems(worldIn, pos, (TileEntityLovoFurnace)tileentity);
	                worldIn.updateComparatorOutputLevel(pos, this);
	            }

	        super.breakBlock(worldIn, pos, state);
	 }
	
	public int getComparatorInputOverride(World worldIn, BlockPos pos) {
	    TileEntity ent = worldIn.getTileEntity(pos);
		if(ent != null && ent instanceof TileEntityLovoFurnace)return Container.calcRedstone(ent);
		return 0;
	}
	
	public boolean isOpaqueCube() {
		return false;
	}
	
	public boolean shouldSideBeRendered(IBlockAccess worldIn, BlockPos pos, EnumFacing side) {
		return true;
	}
	
	public boolean hasComparatorInputOverride() {
		return true;
	}
}
