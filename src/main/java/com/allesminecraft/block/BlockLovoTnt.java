package com.allesminecraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class BlockLovoTnt extends Block{

	public BlockLovoTnt() {
		super(Material.tnt);
		this.setHardness(1.0F);
		this.setResistance(1.0F);
	}

}
