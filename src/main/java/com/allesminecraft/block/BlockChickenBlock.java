package com.allesminecraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockChickenBlock extends Block{

	public BlockChickenBlock() {
		super(Material.gourd);
		this.setHardness(1.0F);
		this.setResistance(2.0F);
		this.setStepSound(SoundType.SLIME);
		this.slipperiness = 0.8F;
	}

}
