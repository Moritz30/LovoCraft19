package com.allesminecraft.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockLovoPortal extends Block{

	public BlockLovoPortal() {
		super(Material.portal);
		this.setHardness(1.F);
		this.setResistance(1.0F);
	
	}

}
