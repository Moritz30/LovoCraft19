package com.allesminecraft;

import net.minecraft.block.Block;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.Item;
import net.minecraft.util.math.AxisAlignedBB;

public class LovoCraftUtils {

	public static void setNames(Object obj, String name) {
		if (obj instanceof Block) {
			Block block = (Block) obj;
			block.setUnlocalizedName(name);
			block.setRegistryName(name);
		} else if (obj instanceof Item) {
			Item item = (Item) obj;
			item.setUnlocalizedName(name);
			item.setRegistryName(name);
		} else {
			throw new IllegalArgumentException("Item or Block required");
		}
	}

	public static boolean consumeInventoryItem(InventoryPlayer playerinventory, Object object) {

		Item item;

		if (object instanceof Block) {
			item = Item.getItemFromBlock((Block) object);
		} else if (object instanceof Item) {
			item = (Item) object;
		} else {
			throw new IllegalArgumentException("Item or Block required");
		}

		int i = getInventorySlotContainItem(playerinventory, item);

		if (i < 0) {
			return false;
		} else {
			if (--playerinventory.mainInventory[i].stackSize <= 0) {
				playerinventory.mainInventory[i] = null;
			}
			return true;
		}
	}

	public static boolean hasItem(InventoryPlayer playerinventory, Object object) {

		Item item;

		if (object instanceof Block) {
			item = Item.getItemFromBlock((Block) object);
		} else if (object instanceof Item) {
			item = (Item) object;
		} else {
			throw new IllegalArgumentException("Item or Block required");
		}

		int i = getInventorySlotContainItem(playerinventory, item);
		return i >= 0;
	}

	public static int getInventorySlotContainItem(InventoryPlayer playerinventory, Item itemIn) {
		for (int i = 0; i < playerinventory.mainInventory.length; ++i) {
			if (playerinventory.mainInventory[i] != null && playerinventory.mainInventory[i].getItem() == itemIn) {
				return i;
			}
		}
		return -1;
	}

	public static AxisAlignedBB fromBounds(double x1, double y1, double z1, double x2, double y2, double z2) {
		double d6 = Math.min(x1, x2);
		double d7 = Math.min(y1, y2);
		double d8 = Math.min(z1, z2);
		double d9 = Math.max(x1, x2);
		double d10 = Math.max(y1, y2);
		double d11 = Math.max(z1, z2);
		return new AxisAlignedBB(d6, d7, d8, d9, d10, d11);
	}
	
	public interface IUpdatePlayerListBox
	{
	    /**
	     * Updates the JList with a new model.
	     */
	    void update();
	}
}
