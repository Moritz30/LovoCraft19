package com.allesminecraft;

import net.minecraft.block.Block;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.allesminecraft.block.BlockChickenBlock;
import com.allesminecraft.block.BlockEffectBlock;
import com.allesminecraft.block.BlockLovoBlock;
import com.allesminecraft.block.BlockLovoBrick;
import com.allesminecraft.block.BlockLovoBrickChiselled;
import com.allesminecraft.block.BlockLovoBrickCracked;
import com.allesminecraft.block.BlockLovoCobblestone;
import com.allesminecraft.block.BlockLovoCobblestoneFired;
import com.allesminecraft.block.BlockLovoFurnace;
import com.allesminecraft.block.BlockLovoOreDim;
import com.allesminecraft.block.BlockLovoOreNether;
import com.allesminecraft.block.BlockLovoPortal;
import com.allesminecraft.block.BlockLovoPortalFrame;
import com.allesminecraft.block.BlockLovoPortalFrameCrystal;
import com.allesminecraft.block.BlockLovoPortalFrameEmpty;
import com.allesminecraft.block.BlockLovoStone;
import com.allesminecraft.block.BlockLovoTnt;


public class LovoCraftBlocks {

	public static Block lovostone;
	public static Block lovocobblestone;
	public static Block lovocobblestonefired;
	public static Block lovobrick;
	public static Block lovobrickchiselled;
	public static Block lovobrickcracked;
	public static Block lovoblock;
	public static Block lovoorenether;
	public static Block lovooredim;
	public static Block lovoportal;
	public static Block lovoportalframe;
	public static Block lovoportalframeempty;
	public static Block lovoportalframecrystal;
	public static Block lovofurnace;
	public static Block lovotnt;
	public static Block effectblock;
	public static Block chickenblock;
	
	public LovoCraftBlocks() {
		init();
		register();
	}

	private void init() {
		lovostone = new BlockLovoStone().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovostone, "lovostone");
		lovocobblestone = new BlockLovoCobblestone().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovocobblestone, "lovocobblestone");
		lovocobblestonefired = new BlockLovoCobblestoneFired().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovocobblestonefired, "lovocobblestonefired");
		lovobrick = new BlockLovoBrick().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovobrick, "lovobrick");
		lovobrickchiselled = new BlockLovoBrickChiselled().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovobrickchiselled, "lovobrickchiselled");
		lovobrickcracked = new BlockLovoBrickCracked().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovobrickcracked, "lovobrickcracked");
		lovoblock = new BlockLovoBlock().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoblock, "lovoblock");
		lovoorenether = new BlockLovoOreNether().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoorenether, "lovoorenether");
		lovooredim = new BlockLovoOreDim().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovooredim, "lovooredim");
		lovoportal = new BlockLovoPortal().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoportal, "lovoportal");
		lovoportalframe = new BlockLovoPortalFrame().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoportalframe, "lovoportalframe");
		lovoportalframeempty = new BlockLovoPortalFrameEmpty().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoportalframeempty, "lovoportalframeempty");
		lovoportalframecrystal = new BlockLovoPortalFrameCrystal().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoportalframecrystal, "lovoportalframecrystal");
		lovofurnace = new BlockLovoFurnace().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovofurnace, "lovofurnace");
		lovotnt = new BlockLovoTnt().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovotnt, "lovotnt");
		effectblock = new BlockEffectBlock().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(effectblock, "effectblock");
		chickenblock = new BlockChickenBlock().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(chickenblock, "chickenblock");
	}

	private void register() {
	this.registerBlock(lovostone);
	this.registerBlock(lovocobblestone);
	this.registerBlock(lovocobblestonefired);
	this.registerBlock(lovobrick);
	this.registerBlock(lovobrickchiselled);
	this.registerBlock(lovobrickcracked);
	this.registerBlock(lovoblock);
	this.registerBlock(lovoorenether);
	this.registerBlock(lovooredim);
	this.registerBlock(lovoportal);
	this.registerBlock(lovoportalframe);
	this.registerBlock(lovoportalframeempty);
	this.registerBlock(lovoportalframecrystal);
	this.registerBlock(lovofurnace);
	this.registerBlock(lovotnt);
	this.registerBlock(effectblock);
	this.registerBlock(chickenblock);

	}
	
	private void registerBlock(Block block) {
	GameRegistry.register(block);
	GameRegistry.register(new ItemBlock(block).setUnlocalizedName(block.getUnlocalizedName()).setRegistryName(block.getRegistryName()));
	}
}
