package com.allesminecraft.other.gui;



import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;

import com.allesminecraft.item.ItemFurnaceUpgrade;

public class Containerlovo extends Container{

	public IInventory inv;
	
	public Containerlovo(IInventory inv,EntityPlayer pl) {
		this.inv = inv;
		
		int i = 0;
        int v = 9;
        int j = 0;
		
		super.addSlotToContainer(new Slot(inv, 0, 148, 36));
		
		for (int x = 0; x < 3; ++x)
        {
            for (int y = 0; y < 3; ++y)
            {
            	super.addSlotToContainer(new Slot(inv, (y + (x * 3)) + 1, 53 + y * 18, 17 + x * 18));
            }
        }
	         
		super.addSlotToContainer(new Slot(inv, 10, 8, 35));
		super.addSlotToContainer(new Slot(inv, 11, 8, 53));
		
		for (int x = 0; x < 4; ++x)
        {
            for (int y = 0; y < 4; ++y)
            {
            	super.addSlotToContainer(new Slot(inv, (y + (x * 4)) + 12, 177 + y * 18, 9 + x * 18));
            }
        }
		
		super.addSlotToContainer(new Slot(inv, 28, 8, 10){
			@Override
			public boolean isItemValid(ItemStack stack) {
				return stack != null && stack.getItem() instanceof ItemFurnaceUpgrade;
			}
		});
		
	         for (i = 0; i < 3; ++i)
	         {
	             for (j = 0; j < 9; ++j)
	             {
	             	super.addSlotToContainer(new Slot(pl.inventory, (j + (i * 9)) + v, 8 + j * 18, 84 + i * 18));
	             }
	         }
	  
	         for (i = 0; i < 9; ++i)
	         {
	         	super.addSlotToContainer(new Slot(pl.inventory, i, 8 + i * 18, 142));
	         }
	}
	
	@Override
	public boolean canInteractWith(EntityPlayer playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
		 ItemStack itemstack = null;
	     Slot slot = (Slot)this.inventorySlots.get(index);

	        if (slot != null && slot.getHasStack())
	        {
	            ItemStack itemstack1 = slot.getStack();
	            itemstack = itemstack1.copy();

	            if (index == 0)
	            {
	                if (!this.mergeItemStack(itemstack1, 28, 36 + 28, false))
	                {
	                    return null;
	                }

	                slot.onSlotChange(itemstack1, itemstack);
	            }
	            else if (index > 10 && index < 28)
	            {
	                    if (!this.mergeItemStack(itemstack1, 36, 36 + 28, true))
	                    {
	                        return null;
	                    }
	                    
	            }else if (index > 0 && index < 12)
	            {
	                if (!this.mergeItemStack(itemstack1, 28, 36 + 28, false))
	                {
	                    return null;
	                }

	                slot.onSlotChange(itemstack1, itemstack);
	            }
	            else if (!this.mergeItemStack(itemstack1, 1, 28, true))
	            {
	                return null;
	            }

	            if (itemstack1.stackSize == 0)
	            {
	                slot.putStack((ItemStack)null);
	            }
	            else
	            {
	                slot.onSlotChanged();
	            }

	            if (itemstack1.stackSize == itemstack.stackSize)
	            {
	                return null;
	            }

	            slot.onPickupFromSlot(playerIn, itemstack1);
	        }

	        return itemstack;
	}
	
	@Override
	protected void retrySlotClick(int p_75133_1_, int p_75133_2_, boolean p_75133_3_, EntityPlayer p_75133_4_) {}
}
