package com.allesminecraft.other;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.other.gui.Containerlovo;
import com.allesminecraft.other.gui.GuiLovoFurnace;



public class GuiHandler implements IGuiHandler {

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		BlockPos p = new BlockPos(x,y,z);
		TileEntity ent = world.getTileEntity(p);
		switch (ID) {
		case LovoCraft.GUI_LOVOFURNACE:
			return new Containerlovo((IInventory) ent, player);
		default:
			return null;
		}
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world,
			int x, int y, int z) {
		BlockPos p = new BlockPos(x,y,z);
		TileEntity ent = world.getTileEntity(p);
		switch (ID) {
		case LovoCraft.GUI_LOVOFURNACE:
			return new GuiLovoFurnace(player, (IInventory) ent, new Containerlovo((IInventory) ent, player));
		default:
			return null;
		}
	}

}
