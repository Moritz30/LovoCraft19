package com.allesminecraft.other;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativTabLovocraft extends CreativeTabs {

	public CreativTabLovocraft() {
		super("LovoCraft");
		this.setBackgroundImageName("item_search.png");
	}

	@Override
	public Item getTabIconItem() {

		return LovoCraftItems.lovocrystal;
	}

	@Override
	public boolean hasSearchBar() {
		return true;
	}
}
