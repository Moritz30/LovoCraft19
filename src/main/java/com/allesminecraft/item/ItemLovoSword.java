package com.allesminecraft.item;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemStack;

import com.allesminecraft.other.ItemSwordSettings;

public class ItemLovoSword extends ItemSwordSettings {

	public ItemLovoSword(ToolMaterial material) {
		super(material, 8.0F);

	}

	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player,
			Entity entity) {
		if (entity instanceof EntityLivingBase) {
			entity.setFire(5);
		}
		return super.onLeftClickEntity(stack, player, entity);
	}

}
