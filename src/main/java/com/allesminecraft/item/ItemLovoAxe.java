package com.allesminecraft.item;

import com.allesminecraft.other.ItemAxeSettings;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemStack;

public class ItemLovoAxe extends ItemAxeSettings {

	public ItemLovoAxe(ToolMaterial material) {
		super(material, 1.0F);
	}

	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player,
			Entity entity) {
		if (entity instanceof EntityLivingBase) {
			entity.setFire(5);
		}
		return super.onLeftClickEntity(stack, player, entity);
	}
}
