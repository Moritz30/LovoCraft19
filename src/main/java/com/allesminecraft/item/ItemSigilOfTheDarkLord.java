package com.allesminecraft.item;

import java.util.HashMap;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.item.Item;



public class ItemSigilOfTheDarkLord extends Item {

	private Random r = new Random();
	private static HashMap<Block, Integer> blocks = new HashMap<Block, Integer>();

	public ItemSigilOfTheDarkLord() {
		super();
		this.setMaxStackSize(1);
		this.setContainerItem(this);
	}
/*
	public static void init() {
		blocks.put(LovoCraft.lovostone, 1);
		blocks.put(Blocks.stone, 1);
		blocks.put(Blocks.obsidian, 8);
		blocks.put(Blocks.dirt, 4);
		blocks.put(Blocks.grass, 4);

	}

	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn,
			EntityPlayer playerIn) {
		if (!playerIn.isSneaking()) {
			MeteoritePlacer meteoritePlacer = new MeteoritePlacer(
					new StandardSettings(playerIn));
			meteoritePlacer.spawn();

			// meteoritePlacer.spawn(worldIn, new BlockPos(playerIn.posX+42,
			// playerIn.posY, playerIn.posZ));
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX,
			// (int)playerIn.posY, (int)playerIn.posZ+42);
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX-42,
			// (int)playerIn.posY, (int)playerIn.posZ);
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX,
			// (int)playerIn.posY, (int)playerIn.posZ-42);
			//
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX+42,
			// (int)playerIn.posY, (int)playerIn.posZ+42);
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX-42,
			// (int)playerIn.posY, (int)playerIn.posZ+42);
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX-42,
			// (int)playerIn.posY, (int)playerIn.posZ+42);
			//
			// meteoritePlacer.spawn(worldIn, (int)playerIn.posX-42,
			// (int)playerIn.posY, (int)playerIn.posZ-42);
		}
		return super.onItemRightClick(itemStackIn, worldIn, playerIn);

	}

	@Override
	public boolean hasEffect(ItemStack stack) {
		return true;
	}

	public boolean onItemUse(ItemStack stack, EntityPlayer playerIn,
			final World worldIn, final BlockPos pos, EnumFacing side,
			float hitX, float hitY, float hitZ) {
		if (playerIn.isSneaking()) {

			if (!worldIn.isRemote) {
				if (this.blocks.containsKey(worldIn.getBlockState(pos)
						.getBlock())) {
					new Runner(worldIn, playerIn, pos, 2);
					worldIn.setBlockState(pos, Blocks.stone.getDefaultState());
				}
			}
		}
		return super.onItemUse(stack, playerIn, worldIn, pos, side, hitX, hitY,
				hitZ);
	}

	private List<BlockPos> getSphere(BlockPos center, int radius) {

		List<BlockPos> locs = new ArrayList<BlockPos>();
		for (double y = -radius; y <= radius; y += 1D) {
			for (double x = -radius; x <= radius; x += 1D) {
				for (double z = -radius; z <= radius; z += 1D) {
					if (x * x + z * z + y * y > radius * radius)
						continue;

					BlockPos pos = center;
					locs.add(center.add(x, y, z));
					center = pos;
				}
			}
		}
		Collections.shuffle(locs);
		return locs;
	}

	private class Runner implements IProcess {

		private boolean isDead = false;
		private World world;
		private BlockPos center;
		private int radius;
		int counter = 0;
		private Block type;
		private List<BlockPos> list;

		public Runner(World world, EntityPlayer player, BlockPos center,
				int radius) {
			this.world = world;
			this.center = center;
			this.radius = radius;
			this.list = getSphere(center, radius);
			this.type = world.getBlockState(center).getBlock();
			player.attackEntityFrom(LovoCraft.sigilDamageSource,
					2 * blocks.get(type));
			ProcessHandler.addProcess(this);
		}

		@Override
		public void updateProcess() {
			if (counter < list.size()) {
				if (world.getBlockState(list.get(counter)).getBlock() == type) {
					world.setBlockState(list.get(counter),
							Blocks.stone.getDefaultState());
				}
				counter++;
			} else {
				this.isDead = true;
			}
		}

		@Override
		public boolean isDead() {
			return isDead;
		}

	}
	/*
	 * @Override public ItemStack onItemRightClick(ItemStack itemstack, World
	 * world, EntityPlayer playerIn) { if(!playerIn.isSneaking())hier das mit
	 * dem Gui �ffnen rein...* return itemstack; }
	 */
}
