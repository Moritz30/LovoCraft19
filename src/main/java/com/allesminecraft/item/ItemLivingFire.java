package com.allesminecraft.item;

import java.util.Random;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.allesminecraft.LovoCraft;


public class ItemLivingFire extends Item {

	public ItemLivingFire() {
		super();
	}
/*
	public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn,
			EntityPlayer playerIn) {
		boolean flag = playerIn.capabilities.isCreativeMode;
	

		Random random = new Random();
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 5; j++) {

				for (int k = 0; k <= 1; ++k) {
					try {
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() - 1.0F),
								(double) ((float) j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) -i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() - 1.0F),
								(double) ((float) -j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() - 1.0F),
								(double) ((float) -j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) -i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() - 1.0F),
								(double) ((float) j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() + 1.0F),
								(double) ((float) j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) -i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() + 1.0F),
								(double) ((float) -j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() + 1.0F),
								(double) ((float) -j + random.nextFloat()) - 0.5D,
								new int[0]);
						spawnParticle(
								worldIn,
								EnumParticleTypes.FLAME,
								playerIn.posX,
								playerIn.posY + 2,
								playerIn.posZ,
								(double) ((float) -i + random.nextFloat()) - 0.5D,
								(double) ((float) k - random.nextFloat() + 1.0F),
								(double) ((float) j + random.nextFloat()) - 0.5D,
								new int[0]);
					} catch (NoSuchMethodError ex) {
					}
				}

			}
		}
		for (Object obj : worldIn.getEntitiesWithinAABBExcludingEntity(
				playerIn, AxisAlignedBB
						.fromBounds(playerIn.posX - 5, playerIn.posY - 5,
								playerIn.posZ - 5, playerIn.posX + 5,
								playerIn.posY + 5, playerIn.posZ + 5))) {
			if (obj instanceof EntityLivingBase) {
				EntityLivingBase entity = (EntityLivingBase) obj;
				entity.setFire(4);
			}
		}

		if (!flag) {
			playerIn.inventory.consumeInventoryItem(LovoCraft.livingfire);
		}
		return itemStackIn;

	}

	@SideOnly(Side.CLIENT)
	public void spawnParticle(World worldIn, EnumParticleTypes particleType,
			double xCoord, double yCoord, double zCoord, double xOffset,
			double yOffset, double zOffset, int[] p_175688_14_) {
		worldIn.spawnParticle(particleType, xCoord, yCoord, zCoord, xOffset,
				yOffset, zOffset, p_175688_14_);
	}
*/
}