package com.allesminecraft.item;

import com.allesminecraft.LovoCraft;

import net.minecraft.entity.Entity;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class ItemLovoArmor extends ItemArmor {
	public ItemLovoArmor(ArmorMaterial material, EntityEquipmentSlot equipmentSlotIn) {
		super(material, 0, equipmentSlotIn);
		this.setCreativeTab(LovoCraft.tab);
	}
	public String getArmorTexture(ItemStack stack, Entity entity, EntityEquipmentSlot slot, String type) {
		if (slot == EntityEquipmentSlot.HEAD || slot == EntityEquipmentSlot.CHEST || slot == EntityEquipmentSlot.FEET) {
			return LovoCraft.MODID + ":textures/models/armor/lovo_layer_1.png";
		} else if (slot == EntityEquipmentSlot.LEGS) {
			return LovoCraft.MODID + ":textures/models/armor/lovo_layer_2.png";
		} else {
			return null;
		}
	}
}
