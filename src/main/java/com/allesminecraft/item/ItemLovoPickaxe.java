package com.allesminecraft.item;

import java.util.HashMap;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftBlocks;

public class ItemLovoPickaxe extends ItemPickaxe {
	public static boolean x = false;
	public static HashMap<Item, ItemStack> blocks = new HashMap<Item, ItemStack>();
	public ItemLovoPickaxe(ToolMaterial material) {
		super(material);
		init();
	}
	public static void init() {
		blocks.put(Item.getItemFromBlock(Blocks.stone), new ItemStack(Blocks.stone));
		blocks.put(Item.getItemFromBlock(Blocks.cobblestone), new ItemStack(Blocks.stone));
		blocks.put(Item.getItemFromBlock(Blocks.gold_ore), new ItemStack(Items.gold_ingot));
		blocks.put(Item.getItemFromBlock(Blocks.iron_ore), new ItemStack(Items.iron_ingot));
		blocks.put(Item.getItemFromBlock(LovoCraftBlocks.lovostone), new ItemStack(LovoCraftBlocks.lovostone));
		//blocks.put(Item.getItemFromBlock(LovoCraftBlocks.lovocobblestone), new ItemStack(LovoCraft.lovostone));
	}
	
	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
		if(entity instanceof EntityLivingBase){
			entity.setFire(5);
		}
		return super.onLeftClickEntity(stack, player, entity);
	}
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn, final World worldIn, final BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
		if (!worldIn.isRemote) {
//			if (worldIn.getBlockState(pos).getBlock() instanceof BlockLovoPortalFrame) {
//				worldIn.setBlockState(pos,LovoCraft.lovoportalframeempty.getDefaultState());
//			}
		}
		return super.onItemUse(stack, playerIn, worldIn, pos, null, side, hitX, hitY,
				hitZ);
	}
}
