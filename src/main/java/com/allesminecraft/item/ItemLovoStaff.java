package com.allesminecraft.item;

import java.util.List;
import java.util.Random;

import com.allesminecraft.LovoCraftUtils;
import com.mojang.realmsclient.gui.ChatFormatting;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.entity.projectile.EntityLargeFireball;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemLovoStaff extends Item {

	public ItemLovoStaff() {
		super();
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void addInformation(ItemStack stack, EntityPlayer playerIn,
			List tooltip, boolean advanced) {
		switch (getNBT(stack, "mode")) {
		case 0:
			tooltip.add("Mode: " + ChatFormatting.RED + "Fireball");
			break;
		case 1:
			tooltip.add("Mode: " + ChatFormatting.DARK_RED + "Flamethrower");
			break;
		case 2:
			tooltip.add("Mode: " + ChatFormatting.YELLOW + "Lighter");
			break;
		case 42:
			tooltip.add("Mode: " + ChatFormatting.GOLD + "POTATOMODE");
			break;
		}

		tooltip.add("Fuel: " + getNBT(stack, "fuel"));

	}

	@Override
	public void onUpdate(ItemStack stack, World worldIn, Entity entityIn,
			int itemSlot, boolean isSelected) {
		if (stack.getTagCompound() == null) {
			stack.setTagCompound(new NBTTagCompound());
		}
		NBTTagCompound tag = stack.getTagCompound();
		if (!tag.hasKey("mode")) {
			stack.getTagCompound().setInteger("mode", 0);
		}
		if (!tag.hasKey("fuel")) {
			stack.getTagCompound().setInteger("fuel", 200);
		}
	}

	private void setNBT(ItemStack stack, String name, int value) {
		if (stack.hasTagCompound()) {
			NBTTagCompound tag = stack.getTagCompound();
			tag.setInteger(name, value);
		}
	}

	private int getNBT(ItemStack stack, String name) {
		if (stack.hasTagCompound()) {
			NBTTagCompound tag = stack.getTagCompound();
			return tag.getInteger(name);
		}
		return 0;
	}

	public EnumAction getItemUseAction(ItemStack stack) {
		return EnumAction.BLOCK;
	}

	private void click(World world, EntityPlayer entityplayer) {
		world.playSound((EntityPlayer) null, entityplayer.posX,
				entityplayer.posY, entityplayer.posZ,
				SoundEvents.block_comparator_click, SoundCategory.NEUTRAL,
				1.0F, 1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + 0.5F);

	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(ItemStack itemStackIn,
			World worldIn, EntityPlayer entityplayer, EnumHand hand) {
		if (!worldIn.isRemote && entityplayer.isSneaking()) {
			switch (getNBT(itemStackIn, "mode")) {
			case 0:
				entityplayer.addChatMessage(new TextComponentString(
						ChatFormatting.DARK_RED + "Flamethrower"));
				setNBT(itemStackIn, "mode", 1);
				click(worldIn, entityplayer);
				break;
			case 1:
				entityplayer.addChatMessage(new TextComponentString(
						ChatFormatting.YELLOW + "Lighter"));
				setNBT(itemStackIn, "mode", 2);
				click(worldIn, entityplayer);
				break;
			case 2:
				InventoryPlayer playerinventory = entityplayer.inventory;
				if (LovoCraftUtils.hasItem(playerinventory, Items.nether_star)
						&& LovoCraftUtils.hasItem(playerinventory,
								Blocks.beacon)
						&& LovoCraftUtils.hasItem(playerinventory,
								Blocks.diamond_block)) {
					setNBT(itemStackIn, "mode", 42);
					entityplayer.addChatMessage(new TextComponentString(
							ChatFormatting.GOLD + "POTATOMODE"));
					worldIn.playSound((EntityPlayer) null, entityplayer.posX,
							entityplayer.posY, entityplayer.posZ,
							SoundEvents.entity_player_levelup,
							SoundCategory.NEUTRAL, 1.0F,
							1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + 0.5F);
				} else {
					setNBT(itemStackIn, "mode", 0);
					entityplayer.addChatMessage(new TextComponentString(
							ChatFormatting.RED + "Fireball"));
					click(worldIn, entityplayer);
				}
				break;
			case 42:
				entityplayer.addChatMessage(new TextComponentString(
						ChatFormatting.RED + "Fireball"));
				setNBT(itemStackIn, "mode", 0);
				click(worldIn, entityplayer);
				break;
			}
		}
		if (!entityplayer.isSneaking()) {
			int fuel = getNBT(itemStackIn, "fuel");
			switch (getNBT(itemStackIn, "mode")) {
			case 0:
				if (fuel >= 5) {
					boolean flag = entityplayer.capabilities.isCreativeMode;
					if (flag
							|| LovoCraftUtils.hasItem(entityplayer.inventory,
									Items.fire_charge)) {
						if (!worldIn.isRemote) {
							Vec3d look = entityplayer.getLookVec();
							double x = look.xCoord / 10;
							double y = look.yCoord / 10;
							double z = look.zCoord / 10;
							EntityLargeFireball EntityLargeFireball = new EntityLargeFireball(
									worldIn, entityplayer, 1, 1, 1);
							EntityLargeFireball.setPosition(entityplayer.posX
									+ look.xCoord, entityplayer.posY
									+ look.yCoord + 1, entityplayer.posZ
									+ look.zCoord);
							EntityLargeFireball.accelerationX = look.xCoord * 0.2;
							EntityLargeFireball.accelerationY = look.yCoord * 0.2;
							EntityLargeFireball.accelerationZ = look.zCoord * 0.2;
							worldIn.spawnEntityInWorld(EntityLargeFireball);
							worldIn.playSound(
									(EntityPlayer) null,
									entityplayer.posX,
									entityplayer.posY,
									entityplayer.posZ,
									SoundEvents.entity_ghast_shoot,
									SoundCategory.NEUTRAL,
									1.0F,
									1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + 0.5F);
						}
					}

					if (!flag
							&& LovoCraftUtils.hasItem(entityplayer.inventory,
									Items.fire_charge)) {
						LovoCraftUtils.consumeInventoryItem(
								entityplayer.inventory, Items.fire_charge);
						setNBT(itemStackIn, "fuel", fuel - 5);
					}
				}
				break;
			case 1:
				if (fuel >= 1) {
					worldIn.playSound((EntityPlayer) null, entityplayer.posX,
							entityplayer.posY, entityplayer.posZ,
							SoundEvents.block_fire_ambient,
							SoundCategory.NEUTRAL, 1.0F,
							1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + 0.5F);
					Vec3d look = entityplayer.getLookVec();
					double x = look.xCoord / 10;
					double y = look.yCoord / 10;
					double z = look.zCoord / 10;
					Random random = new Random();

					for (int i = 0; i < 100; i++) {
						double rx = random.nextInt(100) / 95;
						double ry = random.nextInt(100) / 95;
						double rz = random.nextInt(100) / 95;
						double xCoord = entityplayer.posX + x * i
								+ random.nextDouble();
						double yCoord = entityplayer.posY + y * i
								+ random.nextDouble();
						double zCoord = entityplayer.posZ + z * i
								+ random.nextDouble();
						worldIn.spawnParticle(
								EnumParticleTypes.FLAME,
								entityplayer.posX + x * i + random.nextDouble(),
								entityplayer.posY + y * i + random.nextDouble()
										+ 1,
								entityplayer.posZ + z * i + random.nextDouble(),
								(x * 10) - rx / 2, (y * 10), (z * 10) - rz / 2);
						worldIn.spawnParticle(
								EnumParticleTypes.FLAME,
								entityplayer.posX + x * i + random.nextDouble(),
								entityplayer.posY + y * i + random.nextDouble()
										+ 1,
								entityplayer.posZ + z * i + random.nextDouble(),
								(x * 10) + rx / 2, (y * 10), (z * 10) + rz / 2);
						worldIn.spawnParticle(
								EnumParticleTypes.SMOKE_NORMAL,
								entityplayer.posX + x * i + random.nextDouble(),
								entityplayer.posY + y * i + random.nextDouble()
										+ 1,
								entityplayer.posZ + z * i + random.nextDouble(),
								(x * 10) - rx / 2, (y * 10), (z * 10) - rz / 2);
						worldIn.spawnParticle(
								EnumParticleTypes.SMOKE_NORMAL,
								entityplayer.posX + x * i + random.nextDouble(),
								entityplayer.posY + y * i + random.nextDouble()
										+ 1,
								entityplayer.posZ + z * i + random.nextDouble(),
								(x * 10) + rx / 2, (y * 10), (z * 10) + rz / 2);

						for (Object obj : worldIn
								.getEntitiesWithinAABBExcludingEntity(
										entityplayer, LovoCraftUtils
												.fromBounds(xCoord - 1,
														yCoord - 1, zCoord - 1,
														xCoord + 1, yCoord + 1,
														zCoord + 1))) {
							if (obj instanceof EntityLivingBase) {
								EntityLivingBase entity = (EntityLivingBase) obj;
								entity.setFire(4);
							}
						}

					}
					if (!entityplayer.capabilities.isCreativeMode) {
						setNBT(itemStackIn, "fuel",
								getNBT(itemStackIn, "fuel") - 1);
					}
				}
				break;

			case 2:

				// entityplayer.addPotionEffect((new
				// PotionEffect(MobEffects.heal, 100, 2, false, false)));
				// entityplayer.addPotionEffect((new
				// PotionEffect(MobEffects.saturation, 100, 2, false, false)));

				break;
			case 42:
				entityplayer.inventory.addItemStackToInventory(new ItemStack(
						Items.baked_potato, 2));
				entityplayer.attackEntityFrom(DamageSource.starve, 4);
				break;
			}
		}
		return super.onItemRightClick(itemStackIn, worldIn, entityplayer, hand);
	}

	@Override
	public EnumActionResult onItemUse(ItemStack stack, EntityPlayer playerIn,
			World worldIn, BlockPos pos, EnumHand hand, EnumFacing side,
			float hitX, float hitY, float hitZ) {
		if ((getNBT(stack, "mode") == 2 && !playerIn.isSneaking())) {
			pos = pos.offset(side);

			if (!playerIn.canPlayerEdit(pos, side, stack)) {
				return EnumActionResult.FAIL;
			} else {
				if (worldIn.isAirBlock(pos)) {
					worldIn.playSound((EntityPlayer) null, playerIn.posX,
							playerIn.posY, playerIn.posZ,
							SoundEvents.item_flintandsteel_use,
							SoundCategory.NEUTRAL, 1.0F,
							1.0F / (itemRand.nextFloat() * 0.4F + 1.2F) + 0.5F);
					worldIn.setBlockState(pos, Blocks.fire.getDefaultState());
				}

				return EnumActionResult.SUCCESS;
			}
		}
		return super.onItemUse(stack, playerIn, worldIn, pos, hand, side, hitX,
				hitY, hitZ);

	}
}
