package com.allesminecraft.tileentity;

public interface ILiquidProvider {

      public float getLimit();
      
      public float getStored();
      
      public void add(float i);
      
      public float get(float i);
}