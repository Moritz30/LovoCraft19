package com.allesminecraft.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntityLockable;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumParticleTypes;
import scala.util.Random;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftBlocks;
import com.allesminecraft.LovoCraftItems;
import com.allesminecraft.LovoCraftUtils.IUpdatePlayerListBox;
import com.allesminecraft.api.ModRegistryUtils;
import com.allesminecraft.item.ItemFurnaceUpgrade;
import com.allesminecraft.other.gui.Containerlovo;

public class TileEntityLovoFurnace extends TileEntityLockable implements 
ILiquidProvider,ISidedInventory,IUpdatePlayerListBox{
	
	ItemStack[] stack = new ItemStack[30];

	private int curenttime;
	private float liquid;
	private int LIMIT = 52;
	private boolean isBurning = false;
	
	@Override
	public int getSizeInventory() {
		return 30;
	}

	@Override
	public ItemStack getStackInSlot(int index) {
		return stack[index];
	}

	@Override
	public ItemStack decrStackSize(int index, int count) {
		if (this.stack[index] != null)
        {
            ItemStack itemstack;

            if (this.stack[index].stackSize <= count)
            {
                itemstack = this.stack[index];
                this.stack[index] = null;
                this.markDirty();
                this.updateCraftig();
                this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
                return itemstack;
            }
            else
            {
                itemstack = this.stack[index].splitStack(count);

                if (this.stack[index].stackSize == 0)
                {
                    this.stack[index] = null;
                }
                this.markDirty();
                this.updateCraftig();
                this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
                return itemstack;
            }
        }
        else
        {
            this.updateCraftig();
            this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
            return null;
        }
	}

	public ItemStack getStackInSlotOnClosing(int index) {
		return stack[index];
	}

	@Override
	public void setInventorySlotContents(int index, ItemStack stack) {
		
        this.stack[index] = stack;	
        if(index == 28){
			curenttime = (int) Math.round(this.stack[28] == null ? curenttime:curenttime/(this.stack[28].stackSize*0.6));
		}
        this.updateCraftig();
        this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
        this.markDirty();
	}

	@Override
	public int getInventoryStackLimit() {
		return 64;
	}

	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return true;
	}

	@Override
	public void openInventory(EntityPlayer player) {}

	@Override
	public void closeInventory(EntityPlayer player) {}

	@Override
	public boolean isItemValidForSlot(int index, ItemStack stack) {
		switch(index){
		case 0:
			return false;
		case 10:
			if(stack.isItemEqual(new ItemStack(Items.blaze_rod)))return true;
			break;
		case 11:
			if(stack.isItemEqual(new ItemStack(LovoCraftItems.lovocrystal)))return true;
			break;
		}
		return false;
	}

	@Override
	public int getField(int id) {
			return curenttime;
	}

	@Override
	public void setField(int id, int value) {
		curenttime = value;
	}

	@Override
	public int getFieldCount() {
		return 1;
	}

	@Override
	public void clear() {
		for(int i = 0;i < stack.length;i++){
			stack[i] = null;
		}
		this.updateCraftig();
	}

	@Override
	public String getName() {
		return "tileentitylovofurnace";
	}

	@Override
	public boolean hasCustomName() {
		return false;
	}

	@Override
	public IChatComponent getDisplayName() {
		return new ChatComponentText(getName());
	}

	/**
	 * @author MrTroble
	 * 0 for Output
	 * 1-9 for Craft
	 * 10 - 11 for fuel
	 * 12 - 27 for Inventory
	 * 28 for Upgrade
	 * 29 for Up coming block (NOT IN CONTAINER)
	 */
	@Override
	public int[] getSlotsForFace(EnumFacing side) {
		switch(side){
		case DOWN:
			return new int[]{0};
		case UP:
			return new int[]{10,11};
		case EAST:
		case NORTH:
		case SOUTH:
		case WEST:
			int[] args = new int[17];
			for(int i = 0; i > args.length;i++){
				args[i] = i+12;
			}
			return args;
		default:
			break;
		}
		return new int[]{};
	}

	@Override
	public boolean canInsertItem(int index, ItemStack itemStackIn, EnumFacing direction) {
		if(itemStackIn == null)return false;
		if((index > 11 && index < 28) || (index == 28 && itemStackIn.getItem() instanceof ItemFurnaceUpgrade)){
			return true;
		}
		switch(index){
		case 0:
			return false;
		case 10:
			if(itemStackIn.isItemEqual(new ItemStack(Items.blaze_rod)))return true;
			break;
		case 11:
			if(itemStackIn.isItemEqual(new ItemStack(LovoCraftItems.lovocrystal)))return true;
			break;
		}
		return false;
	}

	@Override
	public boolean canExtractItem(int index, ItemStack stack, EnumFacing direction) {
		if(index == 0 && direction.equals(EnumFacing.DOWN))return true;
		return false;
	}

	@Override
	public float getLimit() {
		return LIMIT;
	}

	@Override
	public float getStored() {
		return liquid;
	}

	@Override
	public void add(float i){
	    liquid += i;
	}

	@Override
	public float get(float i) {
	    liquid -= i;
		return i;
	}
	
	private static final String 
	NBT_TIME = "curenttime",
	NBT_LEVEL = "liquidlevel",
	LIST_ITEMS = "list",
    BYTE_SLOTS = "slots";
	
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt)
	{
		NBTTagCompound tagCom = pkt.getNbtCompound();
		this.readFromNBT(tagCom);
	}

	@Override
	public Packet getDescriptionPacket()
	{
		NBTTagCompound tagCom = new NBTTagCompound();
		this.writeToNBT(tagCom);
		return new SPacketUpdateTileEntity(pos, getBlockMetadata(), tagCom);
	}
	
	@Override
	public void writeToNBT(NBTTagCompound compound) {
		super.writeToNBT(compound);
		compound.setByte(NBT_TIME,(byte) curenttime);
		compound.setShort(NBT_LEVEL, (short) liquid);

		NBTTagList nbttaglist = new NBTTagList();

	     for (int i = 0; i < this.stack.length; ++i)
	     {
	         if (this.stack[i] != null)
	         {
	             NBTTagCompound nbttagcompound1 = new NBTTagCompound();
	             nbttagcompound1.setByte(BYTE_SLOTS, (byte) i);
	             this.stack[i].writeToNBT(nbttagcompound1);
	             nbttaglist.appendTag(nbttagcompound1);
	         }
	     }

	     compound.setTag(LIST_ITEMS, nbttaglist);
	}

	@Override
	public void readFromNBT(NBTTagCompound compound) {
		super.readFromNBT(compound);
		this.curenttime = compound.getByte(NBT_TIME);
		this.liquid = compound.getShort(NBT_LEVEL);
		
		NBTTagList nbttaglist = compound.getTagList(LIST_ITEMS, 10);
        this.stack = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int b0 = nbttagcompound1.getByte(BYTE_SLOTS);

            if (b0 >= 0 && b0 < this.stack.length)
            {
                this.stack[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
           }
        }
        this.updateCraftig();
	}
	
	public void update() {
		updateLiquid();
		onMatrixChanged();
	}
	
	public float minli;
	
	public void updateCraftig(){
		Object[] out = ModRegistryUtils.isCraftSmelt(new ItemStack[] {stack[1],stack[2],stack[3]}, new ItemStack[] {stack[4],stack[5],stack[6]}, new ItemStack[] {stack[7],stack[8],stack[9]});
	    if(out == null || this.getStored() < 1){
	    	curenttime = 0;
	    	minli = 0;
	    	isBurning = false;
	    }else if(stack[0] == null ? true:hasStackItem(stack[0], ((ItemStack)out[0]).getItem())){
	    	if(stack[0] != null && stack[0].stackSize > 63){
	    		curenttime = 0;
		    	minli = 0;
		    	isBurning = false;
	    		return;
	    	}
	    	minli = (Float) out[1];
		    stack[29] = (ItemStack) out[0];
	    	isBurning = true;
	    }else{
	    	curenttime = 0;
	    	minli = 0;
	    	isBurning = false;
	    }
	}
	
	private void onMatrixChanged(){
		if(isBurning){
			this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, pos.getX()+ 0.25, pos.getY() + 1, pos.getZ()+ 0.25, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 20);
			this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, pos.getX()+ 0.25, pos.getY() + 1, pos.getZ()+ 0.75, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 20);
			this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, pos.getX()+ 0.75, pos.getY() + 1, pos.getZ()+ 0.25, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 20);
			this.worldObj.spawnParticle(EnumParticleTypes.SMOKE_NORMAL, pos.getX()+ 0.75, pos.getY() + 1, pos.getZ()+ 0.75, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 0 + new Random().nextInt(5)/10, 20);

		if(curenttime < (stack[28] == null ? 200:200/(stack[28].stackSize*0.6))){
		curenttime++;
		}else{
			curenttime = 0;
			if(creatItem(stack[29], 0)){
			    for(int i = 1; i < 10;i++){
			    	this.setItemSize(i, 1);
			    }
			    stack[29] = null;
			    this.get(minli);
                this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
		        this.updateCraftig();
		        this.markDirty();
			}
		}
		}
	}
	
	private void updateLiquid(){
		if(hasStackItem(stack[10], Items.blaze_rod) && hasStackItem(stack[11], LovoCraftItems.lovocrystal)){
			int diff = Math.round(this.getLimit() - Math.round(this.getStored()));
			int size = 0;
			if(stack[10].stackSize > stack[11].stackSize){
				size = stack[11].stackSize;
			}else{
				size = stack[10].stackSize;
			}
			if(liquid + size <= this.getLimit()){
			setItemSize(11, size);
			setItemSize(10, size);
			this.add(size);
            this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
			this.markDirty();
			}else if(stack[10].stackSize > diff){
				setItemSize(11, diff);
				setItemSize(10, diff);
				this.add(diff);
                this.worldObj.updateComparatorOutputLevel(pos, LovoCraftBlocks.lovofurnace);
				this.markDirty();
			}else{
 			}
		}
	}

	private boolean creatItem(ItemStack stack,int i){
		if(this.stack[i] != null && stack != null && isItemEqual(stack, this.stack[i])){
			if(this.stack[i].stackSize > 63)return false;
			this.stack[i].stackSize += stack.stackSize;
			return true;
		}else if(this.stack[i] == null){
			this.stack[i] = stack.copy();
			return true;
		}
		return false;
	}
	
	private void setItemSize(int stack,int size){
		if(this.stack[stack] == null)return;
		if(this.stack[stack].stackSize > size){
			this.stack[stack].stackSize -= size;
		}else{
			this.stack[stack] = null;
		}
	}
	
	private boolean hasStackItem(ItemStack stack,Item item){
		if(stack != null && stack.isItemEqual(new ItemStack(item)))return true;
		return false;
	}

	public static boolean isItemEqual(ItemStack stack ,ItemStack stack2){
	       return stack != null && stack2 != null &&  stack.getItem().getUnlocalizedName().equals(stack2.getItem().getUnlocalizedName()); 
	}

	@Override
	public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
		return new Containerlovo(this, playerIn);
	}

	@Override
	public String getGuiID() {
		return "0";
	}
}
