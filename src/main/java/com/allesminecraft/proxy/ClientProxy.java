package com.allesminecraft.proxy;

import com.allesminecraft.LovoCraft;
import com.allesminecraft.LovoCraftBlocks;
import com.allesminecraft.LovoCraftItems;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ClientProxy extends CommonProxy {

	public void registerModels() {
		//Items
		registerItemModel(LovoCraftItems.lovocrystal, 0);
		registerItemModel(LovoCraftItems.lovocrystalcrumbled, 0);
		registerItemModel(LovoCraftItems.lovoiron, 0);
		registerItemModel(LovoCraftItems.lovostick, 0);
		registerItemModel(LovoCraftItems.lovostring, 0);
		registerItemModel(LovoCraftItems.lovostaff, 0);
		registerItemModel(LovoCraftItems.livingfire, 0);
		registerItemModel(LovoCraftItems.sigilofthedarklord, 0);
		registerItemModel(LovoCraftItems.lovofurnaceupgrade, 0);
		
		registerItemModel(LovoCraftItems.lovosword, 0);
		registerItemModel(LovoCraftItems.lovoaxe, 0);
		registerItemModel(LovoCraftItems.lovopickaxe, 0);
		registerItemModel(LovoCraftItems.lovospade, 0);
		registerItemModel(LovoCraftItems.lovohoe, 0);
		registerItemModel(LovoCraftItems.lovobow, 0);
		
		registerItemModel(LovoCraftItems.lovohelmet, 0);
		registerItemModel(LovoCraftItems.lovochestplate, 0);
		registerItemModel(LovoCraftItems.lovoleggings, 0);
		registerItemModel(LovoCraftItems.lovoboots, 0);
		
		//Blocks
		registerBlockModel(LovoCraftBlocks.lovostone, 0);
		registerBlockModel(LovoCraftBlocks.lovocobblestone, 0);
		registerBlockModel(LovoCraftBlocks.lovocobblestonefired, 0);
		registerBlockModel(LovoCraftBlocks.lovobrick, 0);
		registerBlockModel(LovoCraftBlocks.lovobrickchiselled, 0);
		registerBlockModel(LovoCraftBlocks.lovobrickcracked, 0);
		registerBlockModel(LovoCraftBlocks.lovoblock, 0);
		registerBlockModel(LovoCraftBlocks.lovoorenether, 0);
		registerBlockModel(LovoCraftBlocks.lovooredim, 0);
		registerBlockModel(LovoCraftBlocks.lovoportal, 0);
		registerBlockModel(LovoCraftBlocks.lovoportalframe, 0);
		registerBlockModel(LovoCraftBlocks.lovoportalframeempty, 0);
		registerBlockModel(LovoCraftBlocks.lovoportalframecrystal, 0);
		registerBlockModel(LovoCraftBlocks.lovofurnace, 0);
		registerBlockModel(LovoCraftBlocks.lovotnt, 0);
		registerBlockModel(LovoCraftBlocks.effectblock, 0);
		registerBlockModel(LovoCraftBlocks.chickenblock, 0);
	}

	// Items
	private void registerItemModel(Item item, int meta) {
		Minecraft
				.getMinecraft()
				.getRenderItem()
				.getItemModelMesher()
				.register(
						item,
						meta,
						new ModelResourceLocation(LovoCraft.MODID + ":"
								+ item.getUnlocalizedName().substring(5),
								"inventory"));
	}

	// Blocks
	private void registerBlockModel(Block block, int meta) {
		Minecraft
				.getMinecraft()
				.getRenderItem()
				.getItemModelMesher()
				.register(
						Item.getItemFromBlock(block),
						meta,
						new ModelResourceLocation(LovoCraft.MODID + ":"
								+ block.getUnlocalizedName().substring(5),
								"inventory"));
	}
}
