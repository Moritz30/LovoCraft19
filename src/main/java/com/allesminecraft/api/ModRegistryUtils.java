package com.allesminecraft.api;

import java.util.ArrayList;
import java.util.Set;

import net.minecraft.item.ItemStack;

public class ModRegistryUtils {
	
	private static ArrayList<CraftSmeltRecepie> craftlist = new ArrayList<CraftSmeltRecepie>();	
	private static ArrayList<CraftSmeltRecepieShapeless> craftlist2 = new ArrayList<CraftSmeltRecepieShapeless>();
	
	public static void addCraftSmeltRecepie(CraftSmeltRecepie re){
		craftlist.add(re);
	}
	
	public static void addCraftSmeltRecepieShapless(CraftSmeltRecepieShapeless re){
		craftlist2.add(re);
	}
	
	public static Object[] isCraftSmelt(ItemStack[] line1,ItemStack[] line2,ItemStack[] line3){
	    ItemStack[] all = {line1[0],line1[1],line1[2],line2[0],line2[1],line2[2],line3[0],line3[1],line3[2]};
	    Object[] stack = isShaplesCraftSmelt(all);
		if(stack != null){
			return stack;
		}
		for(int i = 0;i < craftlist.size();i++){
			CraftSmeltRecepie re = craftlist.get(i);
			Object[] st = is(line1, line2, line3, re);
			if(st != null){
				return st;
			}
		}
		return null;
	}
	
	private static Object[] is(ItemStack[] line1,ItemStack[] line2,ItemStack[] line3,CraftSmeltRecepie re){
		for(int j = 0;j < 3;j++){
			if((re.getLine1()[j] == null ? line1[j] == null:false) || !re.getLine1()[j].isItemEqual(line1[j])){
				return null;
			}
		}
		for(int j = 0;j < 3;j++){
			if((re.getLine2()[j] == null ? line2[j] != null: !re.getLine2()[j].isItemEqual(line2[j]))){
				return null;
			}
		}
		for(int j = 0;j < 3;j++){
			if((re.getLine3()[j] == null ? line3[j] != null: !re.getLine3()[j].isItemEqual(line3[j]))){
				return null;
			}
		}
		return new Object[] {re.getOutput(),re.liqu};
	}
	
	private static Object[] isShaplesCraftSmelt(ItemStack[] all){
		for(int i = 0;i < craftlist2.size();i++){
			CraftSmeltRecepieShapeless re = craftlist2.get(i);
			    Object[] out = isShapless(all, re);
			    if(out != null)return out;
		}
		return null;
	}
	
	private static Object[] isShapless(ItemStack[] all,CraftSmeltRecepieShapeless re){
		int i = 0;
		int nul = 0;
		for(int j = 0;j < all.length;j++){
			ItemStack sta = all[j];
	    	 if(sta != null){
		     for(int x = 0;x < re.getItemsRequied().length;x++){
		    	 if(!re.getItemsRequied()[x].isItemEqual(sta)){
         	        return null;
		    	 }
		     }
    		 i++;
	    	 }else{
	    		 nul++;
	    	 }
	    		 if(i > re.getItemsRequied().length || nul >= 9){
	    			 return null;
	    		 }
	 		
		}
		return new Object[]{re.getOutput().copy(),re.liqud};
	}
	
}
