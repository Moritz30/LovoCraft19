package com.allesminecraft.api;

import net.minecraft.item.ItemStack;

public class CraftSmeltRecepieShapeless {
	
	private ItemStack[] arr;
	private ItemStack output;
	public float liqud;
	
	public CraftSmeltRecepieShapeless(ItemStack[] arr,ItemStack output,float liquidred) {
		this.arr = arr;
		this.output = output;
		liqud = liquidred;
	}

	public ItemStack[] getItemsRequied(){
		return arr;
	}
	
	public ItemStack getOutput(){
		return output;
	}
}
