package com.allesminecraft;



import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import com.allesminecraft.other.CreativTabLovocraft;
import com.allesminecraft.proxy.CommonProxy;

@Mod(modid = LovoCraft.MODID, version = LovoCraft.VERSION)
public class LovoCraft {
	public static final String MODID = "lovocraft";
	public static final String VERSION = "0.1";
	public static final int GUI_LOVOFURNACE = 0;
	@Instance(MODID)
	public static LovoCraft INSTANCE = new LovoCraft();

	@SidedProxy(serverSide = "com.allesminecraft.proxy.CommonProxy", clientSide = "com.allesminecraft.proxy.ClientProxy", modId = MODID)
	public static CommonProxy PROXY = new CommonProxy();
    public static CreativeTabs tab;
	@EventHandler
	public void preinit(FMLPreInitializationEvent event) {

	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		tab = new CreativTabLovocraft();
		new LovoCraftItems();
		new LovoCraftBlocks();

	}



	@EventHandler
	public void postinit(FMLPostInitializationEvent event) {
		PROXY.registerModels();
	
	}
}
