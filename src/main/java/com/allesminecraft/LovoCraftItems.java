package com.allesminecraft;

import net.minecraft.init.SoundEvents;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemAxe;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemHoe;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.ItemSword;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;

import com.allesminecraft.item.ItemCrumbledLovoCrystal;
import com.allesminecraft.item.ItemFurnaceUpgrade;
import com.allesminecraft.item.ItemLovoArmor;
import com.allesminecraft.item.ItemLovoAxe;
import com.allesminecraft.item.ItemLovoBow;
import com.allesminecraft.item.ItemLovoCrystal;
import com.allesminecraft.item.ItemLovoHoe;
import com.allesminecraft.item.ItemLovoIron;
import com.allesminecraft.item.ItemLovoPickaxe;
import com.allesminecraft.item.ItemLovoSpade;
import com.allesminecraft.item.ItemLovoStaff;
import com.allesminecraft.item.ItemLovoStick;
import com.allesminecraft.item.ItemLovoString;
import com.allesminecraft.item.ItemLovoSword;


public class LovoCraftItems {
	public static ToolMaterial lovocrafttoolmaterial;
	public static ArmorMaterial lovocraftarmormaterial;
	
	public static Item lovocrystal;
	public static Item lovocrystalcrumbled;
	public static Item lovoiron;
	public static Item lovostick;
	public static Item lovostring;
	public static Item lovostaff;
	public static Item livingfire;
	public static Item sigilofthedarklord;
	public static Item lovofurnaceupgrade;

    public static Item lovosword;
	public static Item lovoaxe;
	public static Item lovopickaxe;
	public static Item lovospade;
	public static Item lovohoe;
	public static Item lovobow; 
	
	public static Item lovohelmet;
	public static Item lovochestplate;
	public static Item lovoleggings;
	public static Item lovoboots;

	public LovoCraftItems() {
		init();
		register();
	}

	private void init() {
		lovocrafttoolmaterial = EnumHelper.addToolMaterial("lovocrafttoolmaterial", 3, 600, 5, 3, 25);
		lovocraftarmormaterial = EnumHelper.addArmorMaterial("lovoarmor", "", 33, new int[] {3, 6, 8, 3}, 35, SoundEvents.item_armor_equip_iron);
		
		lovocrystal = new ItemLovoCrystal().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovocrystal, "lovocrystal");
		lovocrystalcrumbled = new ItemCrumbledLovoCrystal().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovocrystalcrumbled, "lovocrystalcrumbled");
		lovoiron = new ItemLovoIron().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoiron, "lovoiron");
		lovostick = new ItemLovoStick().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovostick, "lovostick");
		lovostring = new ItemLovoString().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovostring, "lovostring");
		lovostaff = new ItemLovoStaff().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovostaff, "lovostaff");
		livingfire = new ItemLovoString().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(livingfire, "livingfire");
		sigilofthedarklord = new ItemLovoString().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(sigilofthedarklord, "sigilofthedarklord");
		lovofurnaceupgrade = new ItemFurnaceUpgrade().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovofurnaceupgrade, "lovofurnaceupgrade");
		
		lovosword = new ItemLovoSword(lovocrafttoolmaterial).setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovosword, "lovosword");
		lovoaxe = new ItemLovoAxe(lovocrafttoolmaterial).setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovoaxe, "lovoaxe");
		lovopickaxe = new ItemLovoPickaxe(lovocrafttoolmaterial).setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovopickaxe, "lovopickaxe");
		lovospade = new ItemLovoSpade(lovocrafttoolmaterial).setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovospade, "lovospade");
		lovohoe = new ItemLovoHoe(lovocrafttoolmaterial).setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovohoe, "lovohoe");
		lovobow = new ItemLovoBow().setCreativeTab(LovoCraft.tab);
		LovoCraftUtils.setNames(lovobow, "lovobow");
		
		lovohelmet = new ItemLovoArmor(lovocraftarmormaterial, EntityEquipmentSlot.HEAD);
		LovoCraftUtils.setNames(lovohelmet, "lovohelmet");
		lovochestplate = new ItemLovoArmor(lovocraftarmormaterial, EntityEquipmentSlot.CHEST);
		LovoCraftUtils.setNames(lovochestplate, "lovochestplate");
		lovoleggings = new ItemLovoArmor(lovocraftarmormaterial, EntityEquipmentSlot.LEGS);
		LovoCraftUtils.setNames(lovoleggings, "lovoleggings");
		lovoboots = new ItemLovoArmor(lovocraftarmormaterial, EntityEquipmentSlot.FEET);
		LovoCraftUtils.setNames(lovoboots, "lovoboots");
	}

	private void register() {
		GameRegistry.register(lovocrystal);
		GameRegistry.register(lovocrystalcrumbled);
		GameRegistry.register(lovoiron);
		GameRegistry.register(lovostick);
		GameRegistry.register(lovostring);
		GameRegistry.register(lovostaff);
		GameRegistry.register(livingfire);
		GameRegistry.register(sigilofthedarklord);
		GameRegistry.register(lovofurnaceupgrade);
	
		GameRegistry.register(lovosword);
		GameRegistry.register(lovoaxe);
		GameRegistry.register(lovopickaxe);
		GameRegistry.register(lovospade);
		GameRegistry.register(lovohoe);
		GameRegistry.register(lovobow);
		
		GameRegistry.register(lovohelmet);
		GameRegistry.register(lovochestplate);
		GameRegistry.register(lovoleggings);
		GameRegistry.register(lovoboots);
	}
	

}
